package com.mldong.exception;

import cn.hutool.core.lang.Dict;
import com.mldong.base.ErrEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * 业务异常
 * @author mldong
 * @date 2022-01-06
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ServiceException extends RuntimeException {

    private Integer code;

    private String errorMessage;
    private Dict data = Dict.create();
    public ServiceException(Integer code, String errorMessage,Dict data) {
        super(errorMessage);
        this.code = code;
        this.errorMessage = errorMessage;
        this.data = data;
    }
    public ServiceException(Integer code, String errorMessage) {
        this(code, errorMessage,Dict.create());
    }

    public ServiceException(ErrEnum exception) {
        this(exception.getCode(),exception.getMessage());
    }
    /**
     * 抛出自定义异常
     * @param code
     * @param message
     */
    public static void throwBiz(Integer code, String message) {
        throw new ServiceException(code, message);
    }
    /**
     * 抛出自定义异常
     * @param errEnum
     */
    public static void throwBiz(ErrEnum errEnum) {
        throw new ServiceException(errEnum.getCode(), errEnum.getMessage());
    }
    public static void throwBiz(ErrEnum errEnum,Dict data) {
        throw new ServiceException(errEnum.getCode(), errEnum.getMessage(),data);
    }
    public static void throwBiz(Integer code, String message,Dict data) {
        throw new ServiceException(code, message, data);
    }
}
