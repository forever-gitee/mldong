package com.mldong.auth.handler;

import cn.hutool.core.lang.Dict;

import java.util.List;

/**
 * 关联第三方账号处理接口
 * @author mldong
 * @date 2024/8/19
 */
public interface RelThirdAccountHandler {
    /**
     * 关联
     * @param dict
     * @return
     */
    List<RelThirdAccountModel> relHandle(Dict dict);

    /**
     * 取消关联
     * @param dict
     * @return 获取该关联处理类生成的第三方账号类型
     */
    List<String> unRelHandle(Dict dict);
}
