package com.mldong.auth;

import cn.hutool.core.lang.Dict;

/**
 * 登录处理器
 * @author mldong
 * @date 2024/8/16
 */
public interface LoginHandler {
    /**
     * 登录前处理
     * @param param
     * @return
     */
    Dict preLogin(Dict param);

    /**
     * 登录后处理
     * @param loginUser
     * @return
     */
    LoginUser postLogin(LoginUser loginUser);
}
