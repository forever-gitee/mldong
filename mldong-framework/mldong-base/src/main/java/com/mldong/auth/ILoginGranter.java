package com.mldong.auth;

import cn.hutool.core.lang.Dict;

/**
 * 登录授权接口
 * @author mldong
 * @date 2024/8/19
 */
public interface ILoginGranter {
    /***
     * 登录授权
     * @param param 用户名、密码等登录参数
     * @return 用户信息，即sys_user表信息
     */
    Dict grant(Dict param);
}
