package com.mldong.auth.handler;

import lombok.Data;

/**
 * @author mldong
 * @date 2024/8/19
 */
@Data
public class RelThirdAccountModel {
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 第三方账号
     */
    private String thirdAccount;
    /**
     * 第三方账号类型
     */
    private String thirdType;
}
