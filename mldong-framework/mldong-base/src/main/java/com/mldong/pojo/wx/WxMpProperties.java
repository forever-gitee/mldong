package com.mldong.pojo.wx;

import lombok.Data;

/**
 * 微信公众号配置
 * @author mldong
 * @date 2024/8/19
 */
@Data
public class WxMpProperties {
    /**
     * 设置微信公众号的appid
     */
    private String appid;

    /**
     * 设置微信公众号的Secret
     */
    private String secret;

    /**
     * 设置微信公众号消息服务器配置的token
     */
    private String token;

    /**
     * 设置微信公众号息服务器配置的EncodingAESKey
     */
    private String aesKey;
}
