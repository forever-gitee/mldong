package com.mldong.pojo.wx;

import lombok.Data;

/**
 * 微信支付配置
 * @author mldong
 * @date 2024/8/19
 */
@Data
public class WxPayProperties {
    /**
     * 设置微信公众号或者小程序等的appid
     */
    private String appId;

    /**
     * 微信支付商户号
     */
    private String mchId;

    /**
     * 微信支付商户密钥
     */
    private String mchKey;

    /**
     * 服务商模式下的子商户公众账号ID，普通模式请不要配置，请在配置文件中将对应项删除
     */
    private String subAppId;

    /**
     * 服务商模式下的子商户号，普通模式请不要配置，最好是请在配置文件中将对应项删除
     */
    private String subMchId;

    /**
     * apiclient_cert.p12文件的绝对路径，或者如果放在项目中，请以classpath:开头指定
     */
    private String keyPath;
    /**
     * APIv3密钥
     */
    private String apiV3Key;
    /**
     * 商户私钥文件路径apiclient_key.pem，退款必填
     */
    private String privateKeyPath;
    /**
     * 商户私钥证书文件路径apiclient_cert.pem,退款必填
     */
    private String privateCertPath;
}
