package com.mldong.pojo.wx;

import lombok.Data;

/**
 * 微信开放平台配置
 * @author mldong
 * @date 2024/8/19
 */
@Data
public class WxOpenProperties {
    /**
     * 设置微信三方平台的appid
     */
    private String componentAppId;

    /**
     * 设置微信三方平台的app secret
     */
    private String componentSecret;

    /**
     * 设置微信三方平台的token
     */
    private String componentToken;

    /**
     * 设置微信三方平台的EncodingAESKey
     */
    private String componentAesKey;
}
