package com.mldong.pojo.wx;

import lombok.Data;

/**
 * 企业号、企业微信配置
 * @author mldong
 * @date 2024/8/19
 */
@Data
public class WxCpProperties {
    /**
     * 设置企业微信应用的AgentId
     */
    private Integer agentId;

    /**
     * 设置企业微信应用的Secret
     */
    private String secret;

    /**
     * 设置企业微信应用的token
     */
    private String token;

    /**
     * 设置企业微信应用的EncodingAESKey
     */
    private String aesKey;
}
