package com.mldong.modules.sys.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.mldong.base.CommonPage;
import com.mldong.base.CommonResult;
import com.mldong.base.IdParam;
import com.mldong.base.IdsParam;
import com.mldong.modules.sys.dto.RelThirdAccountPageParam;
import com.mldong.modules.sys.dto.RelThirdAccountParam;
import com.mldong.modules.sys.service.RelThirdAccountService;
import com.mldong.modules.sys.vo.RelThirdAccountVO;
import com.mldong.validation.Groups;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
/**
* <p>
    * 关联的第三方账号 前端控制器
    * </p>
*
* @author mldong
* @since 2024-08-19
*/
@RestController
@Api(tags = "关联的第三方账号管理")
@RequiredArgsConstructor
public class RelThirdAccountController {
    private final RelThirdAccountService relThirdAccountService;
    /**
     * 添加关联的第三方账号
     * @param param
     * @return
     */
    @PostMapping("/sys/relThirdAccount/save")
    @ApiOperation(value = "添加关联的第三方账号")
    @SaCheckPermission("sys:relThirdAccount:save")
    public CommonResult<?> save(@RequestBody @Validated({Groups.Save.class}) RelThirdAccountParam param) {
        relThirdAccountService.save(param);
        return CommonResult.ok();
    }
    /**
     * 删除关联的第三方账号
     * @param param
     * @return
     */
    @PostMapping("/sys/relThirdAccount/remove")
    @ApiOperation(value = "删除关联的第三方账号")
    @SaCheckPermission("sys:relThirdAccount:remove")
    public CommonResult<?> remove(@RequestBody IdsParam param) {
        relThirdAccountService.removeBatchByIds(param.getIds());
        return CommonResult.ok();
    }
    /**
     * 修改关联的第三方账号
     * @param param
     * @return
     */
    @PostMapping("/sys/relThirdAccount/update")
    @ApiOperation(value = "修改关联的第三方账号")
    @SaCheckPermission("sys:relThirdAccount:update")
    public CommonResult<?> update(@RequestBody @Validated({Groups.Update.class}) RelThirdAccountParam param) {
        relThirdAccountService.update(param);
        return CommonResult.ok();
    }
    /**
     * 查询单个关联的第三方账号
     * @param param
     * @return
     */
    @PostMapping("/sys/relThirdAccount/detail")
    @ApiOperation(value = "查询单个关联的第三方账号")
    @SaCheckPermission("sys:relThirdAccount:detail")
    public CommonResult<RelThirdAccountVO> detail(@RequestBody IdParam param) {
        RelThirdAccountVO relThirdAccount = relThirdAccountService.findById(param.getId());
        return CommonResult.data(relThirdAccount);
    }
    /**
     *分页查询关联的第三方账号列表
     * @param param
     * @return
     */
    @PostMapping("/sys/relThirdAccount/page")
    @ApiOperation(value = "分页查询关联的第三方账号列表")
    @SaCheckPermission("sys:relThirdAccount:page")
    public CommonResult<CommonPage<RelThirdAccountVO>> page(@RequestBody RelThirdAccountPageParam param) {
        return CommonResult.data(relThirdAccountService.page(param));
    }
}
