package com.mldong.config;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import com.mldong.context.constant.ConstantContextHolder;
import com.mldong.pojo.wx.WxPayProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 微信支付配置
 * @author mldong
 * @date 2024/8/19
 */
@Configuration
@ConditionalOnProperty(prefix = "wx",name = "pay.enabled",havingValue = "true")
public class WxPayConfiguration {
    @Bean
    public WxPayService wxPayService() {
        WxPayProperties properties = ConstantContextHolder.getWxPayConfig();
        WxPayService wxPayService = new WxPayServiceImpl();
        WxPayConfig config = new WxPayConfig();
        config.setAppId(properties.getAppId());
        config.setMchId(properties.getMchId());
        config.setMchKey(properties.getMchKey());
        config.setSubAppId(properties.getSubAppId());
        config.setSubMchId(properties.getSubMchId());
        config.setApiV3Key(properties.getApiV3Key());
        // apiclient_cert.p12
        config.setKeyPath(properties.getKeyPath());
        // apiclient_key.pem
        config.setPrivateKeyPath(properties.getPrivateKeyPath());
        // apiclient_cert.pem
        config.setPrivateCertPath(properties.getPrivateCertPath());
        wxPayService.setConfig(config);
        return wxPayService;
    }
}
