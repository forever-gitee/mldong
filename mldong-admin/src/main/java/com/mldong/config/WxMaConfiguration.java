package com.mldong.config;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import com.mldong.context.constant.ConstantContextHolder;
import com.mldong.pojo.wx.WxMaProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 微信小程序配置
 * @author mldong
 * @date 2024/8/19
 */
@Configuration
@ConditionalOnProperty(prefix = "wx",name = "ma.enabled",havingValue = "true")
public class WxMaConfiguration {
    @Bean
    public WxMaService wxMaService() {
        WxMaProperties properties = ConstantContextHolder.getWxMaConfig();
        WxMaService maService = new WxMaServiceImpl();
        WxMaDefaultConfigImpl config = new WxMaDefaultConfigImpl();
        config.setAppid(properties.getAppid());
        config.setSecret(properties.getSecret());
        config.setToken(properties.getToken());
        config.setAesKey(properties.getAesKey());
        config.setMsgDataFormat(properties.getMsgDataFormat());
        maService.setWxMaConfig(config);
        return maService;
    }
}
