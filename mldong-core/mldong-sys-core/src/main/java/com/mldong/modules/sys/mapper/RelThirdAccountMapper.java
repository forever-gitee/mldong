package com.mldong.modules.sys.mapper;

import com.mldong.modules.sys.entity.RelThirdAccount;
import com.mldong.modules.sys.vo.RelThirdAccountVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
/**
 * <p>
 * 关联的第三方账号 Mapper 接口
 * </p>
 *
 * @author mldong
 * @since 2024-08-19
 */
@Mapper
public interface RelThirdAccountMapper extends BaseMapper<RelThirdAccount> {
    List<RelThirdAccountVO> selectCustom(IPage<RelThirdAccountVO> page, @Param(Constants.WRAPPER) Wrapper<RelThirdAccount> wrapper);
    RelThirdAccountVO findById(@Param("id") Long id);
}
