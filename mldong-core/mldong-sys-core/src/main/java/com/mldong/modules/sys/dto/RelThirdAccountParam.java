package com.mldong.modules.sys.dto;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.*;
import com.mldong.validation.Groups;
/**
 * <p>
 * 关联的第三方账号
 * </p>
 *
 * @author mldong
 * @since 2024-08-19
 */
@Getter
@Setter
@TableName("sys_rel_third_account")
@ApiModel(value = "RelThirdAccountParam对象", description = "关联的第三方账号")
public class RelThirdAccountParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键", required = true)
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @NotNull(message = "主键不能为空", groups = {Groups.Update.class})
    private Long id;

    @ApiModelProperty(value = "用户ID", required = true)
    @NotNull(message = "用户ID不能为空")
    private Long userId;

    @ApiModelProperty(value = "第三方账号", required = true)
    @NotBlank(message = "第三方账号不能为空")
    private String thirdAccount;

    @ApiModelProperty(value = "第三方账号类型", required = true)
    @NotBlank(message = "第三方账号类型不能为空")
    private String thirdType;


}
