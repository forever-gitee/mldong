package com.mldong.modules.sys.service;

import com.mldong.base.CommonPage;
import com.mldong.modules.sys.dto.RelThirdAccountPageParam;
import com.mldong.modules.sys.dto.RelThirdAccountParam;
import com.mldong.modules.sys.vo.RelThirdAccountVO;
import com.mldong.modules.sys.entity.RelThirdAccount;
import com.baomidou.mybatisplus.extension.service.IService;
/**
 * <p>
 * 关联的第三方账号 服务类
 * </p>
 *
 * @author mldong
 * @since 2024-08-19
 */
public interface RelThirdAccountService extends IService<RelThirdAccount> {
    /**
    * 添加关联的第三方账号
    * @param param
    * @return
    */
    boolean save(RelThirdAccountParam param);

    /**
    * 更新关联的第三方账号
    * @param param
    * @return
    */
    boolean update(RelThirdAccountParam param);

    /**
    * 自定义分页查询关联的第三方账号
    * @param param
    * @return
    */
    CommonPage<RelThirdAccountVO> page(RelThirdAccountPageParam param);
    /**
    * 通过id查询
    * @param id
    * @return
    */
    RelThirdAccountVO findById(Long id);
}
