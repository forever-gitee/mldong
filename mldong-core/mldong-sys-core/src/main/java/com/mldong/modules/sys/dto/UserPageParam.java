package com.mldong.modules.sys.dto;

import com.mldong.base.PageParam;
import com.mldong.base.YesNoEnum;
import com.mldong.modules.sys.entity.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
// ==
public class UserPageParam extends PageParam<User> {
    @ApiModelProperty(value = "仅查询普通管理员")
    private YesNoEnum onlyCommonAdmin = YesNoEnum.YES;
}