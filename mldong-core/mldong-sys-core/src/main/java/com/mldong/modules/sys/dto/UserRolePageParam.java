package com.mldong.modules.sys.dto;

import com.mldong.base.PageParam;
import com.mldong.base.YesNoEnum;
import com.mldong.modules.sys.entity.UserRole;
import com.mldong.modules.sys.enums.AdminTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * <p>
 * r_用户角色关系
 * </p>
 *
 * @author mldong
 * @since 2022-09-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="UserRolePageParam对象", description="r_用户角色关系分页参数实体类")
public class UserRolePageParam extends PageParam<UserRole> {
    @ApiModelProperty(value = "角色ID")
    private Long roleId;
    @ApiModelProperty(value = "管理员类型<sys_admin_type>")
    private Object m_IN_adminType = AdminTypeEnum.COMMON_ADMIN.getCode().toString();
}