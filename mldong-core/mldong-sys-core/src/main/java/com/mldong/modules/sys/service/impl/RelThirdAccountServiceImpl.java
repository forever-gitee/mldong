package com.mldong.modules.sys.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mldong.base.CommonPage;
import com.mldong.base.YesNoEnum;
import com.mldong.modules.sys.dto.RelThirdAccountPageParam;
import com.mldong.modules.sys.dto.RelThirdAccountParam;
import com.mldong.modules.sys.vo.RelThirdAccountVO;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import com.mldong.modules.sys.entity.RelThirdAccount;
import com.mldong.modules.sys.mapper.RelThirdAccountMapper;
import com.mldong.modules.sys.service.RelThirdAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
/**
 * <p>
 * 关联的第三方账号 服务实现类
 * </p>
 *
 * @author mldong
 * @since 2024-08-19
 */
@Service
public class RelThirdAccountServiceImpl extends ServiceImpl<RelThirdAccountMapper, RelThirdAccount> implements RelThirdAccountService {
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean save(RelThirdAccountParam param) {
        param.setId(null);
        RelThirdAccount relThirdAccount = new RelThirdAccount();
        BeanUtil.copyProperties(param, relThirdAccount);
        return super.save(relThirdAccount);
    }

    @Override
    public boolean update(RelThirdAccountParam param) {
        RelThirdAccount relThirdAccount = new RelThirdAccount();
        BeanUtil.copyProperties(param, relThirdAccount);
        return super.updateById(relThirdAccount);
    }

    @Override
    public CommonPage<RelThirdAccountVO> page(RelThirdAccountPageParam param) {
        IPage<RelThirdAccountVO> page = param.buildMpPage();
        QueryWrapper queryWrapper = param.buildQueryWrapper();
        queryWrapper.eq("t.is_deleted",YesNoEnum.NO);
        List<RelThirdAccountVO> list = baseMapper.selectCustom(page, queryWrapper);
        page.setRecords(list);
        return CommonPage.toPage(page);
    }
    @Override
    public RelThirdAccountVO findById(Long id) {
        return baseMapper.findById(id);
    }
}
