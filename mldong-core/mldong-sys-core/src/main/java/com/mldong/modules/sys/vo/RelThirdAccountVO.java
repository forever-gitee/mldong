package com.mldong.modules.sys.vo;

import com.mldong.modules.sys.entity.RelThirdAccount;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p>
 * 关联的第三方账号
 * </p>
 *
 * @author mldong
 * @since 2024-08-19
 */
@Data
@ApiModel(value = "RelThirdAccountVO对象", description = "关联的第三方账号VO")
public class RelThirdAccountVO extends RelThirdAccount {
}
