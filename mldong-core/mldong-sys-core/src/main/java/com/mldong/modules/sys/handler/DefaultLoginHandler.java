package com.mldong.modules.sys.handler;

import cn.hutool.core.lang.Dict;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mldong.auth.LoginHandler;
import com.mldong.auth.LoginUser;
import com.mldong.modules.sys.entity.RelThirdAccount;
import com.mldong.modules.sys.mapper.RelThirdAccountMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author mldong
 * @date 2024/8/19
 */
@Component
@RequiredArgsConstructor
public class DefaultLoginHandler implements LoginHandler {
    private final RelThirdAccountMapper relThirdAccountMapper;
    @Override
    public Dict preLogin(Dict param) {
        return param;
    }

    @Override
    public LoginUser postLogin(LoginUser loginUser) {
        // 关联账号数据
        List<RelThirdAccount> relThirdAccountList = relThirdAccountMapper.selectList(
                Wrappers.lambdaQuery(RelThirdAccount.class)
                        .eq(RelThirdAccount::getUserId,loginUser.getId())
        );
        List<String> thirdTypeList = relThirdAccountList.stream().map(item->{
            return item.getThirdType();
        }).collect(Collectors.toList());
        loginUser.setRelThirdAccountTypeList(thirdTypeList);
        return loginUser;
    }
}
